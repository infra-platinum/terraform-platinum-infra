locals {
  docker_host_image_name    = "dockerhost"
  docker_host_image_version = "21Q4"

  nomad_image_name    = "nomad"
  nomad_image_version = "21Q4"
  nomad_snapshot_name = "${local.nomad_image_name}-${local.nomad_image_version}"

  nomad_server_count = 1
  nomad_node_count   = 2

  nomad_node_ipv4_prefix = "10.120.0.0/16"

  nomad_dns_names           = [for i in range(local.nomad_node_count) : "node${i}.nomad.platinum.${var.dns_zone}"]
  consul_dns_names          = [for i in range(local.nomad_node_count) : "node${i}.consul.platinum.${var.dns_zone}"]
  nomad_internal_dns_names  = [for i in range(local.nomad_node_count) : "n${i}.nomad.i.platinum.${var.dns_zone}"]
  consul_internal_dns_names = [for i in range(local.nomad_node_count) : "n${i}.consul.i.platinum.${var.dns_zone}"]
  nomad_ipv4_addrs          = [for i in range(local.nomad_node_count) : cidrhost(local.nomad_node_ipv4_prefix, i + 10)]

  nomad_main_server_dns_name = local.nomad_dns_names[0]

  infra_dir = "/var/local/opt/infra"
}
