data_dir  = "/opt/consul/data"
server     = false
datacenter = "dc1"
log_level  = "INFO"
retry_join = ["${server_node_address}"]

client_addr = "0.0.0.0"
bind_addr   = "0.0.0.0"
advertise_addr = "${node_ip}"

service {
  id      = "dns"
  name    = "dns"
  tags    = ["primary"]
  address = "localhost"
  port    = 8600
  check {
    id       = "dns"
    name     = "Consul DNS TCP on port 8600"
    tcp      = "localhost:8600"
    interval = "10s"
    timeout  = "1s"
  }
}
