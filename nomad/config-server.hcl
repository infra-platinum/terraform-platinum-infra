# Full configuration options can be found at https://www.nomadproject.io/docs/configuration

data_dir = "/opt/nomad/data"
bind_addr = "0.0.0.0"
log_level  = "INFO"

server {
  enabled = true
  bootstrap_expect = 1
}

client {
  enabled = true
  servers = ["127.0.0.1"]
}

advertise {
  http = "${node_ip}"
  rpc  = "${node_ip}"
  serf = "${node_ip}"
}

#acl {
#  enabled = true
#}