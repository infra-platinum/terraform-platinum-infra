
data "cloudflare_zone" "zone" {
  name = var.dns_zone
}

resource "cloudflare_record" "nomad_nodes_a" {
  count   = local.nomad_node_count
  zone_id = data.cloudflare_zone.zone.id
  name    = local.nomad_dns_names[count.index]
  value   = hcloud_server.nomad_node[count.index].ipv4_address
  type    = "A"
  ttl     = 300
}

resource "cloudflare_record" "nomad_nodes_aaaa" {
  count   = local.nomad_node_count
  zone_id = data.cloudflare_zone.zone.id
  name    = local.nomad_dns_names[count.index]
  value   = hcloud_server.nomad_node[count.index].ipv6_address
  type    = "AAAA"
  ttl     = 300
}

resource "cloudflare_record" "consul_nodes_a" {
  count   = local.nomad_node_count
  zone_id = data.cloudflare_zone.zone.id
  name    = local.consul_dns_names[count.index]
  value   = hcloud_server.nomad_node[count.index].ipv4_address
  type    = "A"
  ttl     = 300
}

resource "cloudflare_record" "consul_nodes_aaaa" {
  count   = local.nomad_node_count
  zone_id = data.cloudflare_zone.zone.id
  name    = local.consul_dns_names[count.index]
  value   = hcloud_server.nomad_node[count.index].ipv6_address
  type    = "AAAA"
  ttl     = 300
}

resource "cloudflare_record" "nomad_nodes_internal_a" {
  count   = local.nomad_node_count
  zone_id = data.cloudflare_zone.zone.id
  name    = local.nomad_internal_dns_names[count.index]
  value   = local.nomad_ipv4_addrs[count.index]
  type    = "A"
  ttl     = 60
}
