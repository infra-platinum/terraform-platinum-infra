
resource "hcloud_network" "nomad_network" {
  name     = "nomad-network"
  ip_range = local.nomad_node_ipv4_prefix
}
